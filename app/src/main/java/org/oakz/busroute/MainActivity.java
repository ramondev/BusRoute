package org.oakz.busroute;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

/*   public void sendCoordinates (View view) {


        // ESTA CLASSE ABRE AS COORDENADAS DIRETAMENTE NO APLICATIVO DO GOOGLE MAPS
        // PARA USAR ELA PRECISA CRIAR UM BOTÃO COM A FUNÇÃO onClick:"sendCoordinates"


        EditText longitude = (EditText) findViewById(R.id.longitude);
        EditText latitude = (EditText) findViewById(R.id.latitude);

        double long_coord = Double.parseDouble(longitude.getText().toString());
        double lati_coord = Double.parseDouble(latitude.getText().toString());

        System.out.println(long_coord);
        System.out.println(lati_coord);

        Uri gmmIntentUri = Uri.parse("geo:" + long_coord + "," + lati_coord);

        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        // Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps");

        // Attempt to start an activity that can handle the Intent
        startActivity(mapIntent);


    }   */

    public void toTheMapActivity (View view) {

        //EditText editTextLat = (EditText)findViewById(R.id.latitude);
        //String textLatitude = editTextLat.getText().toString();

        //EditText editTextLng = (EditText)findViewById(R.id.longitude);
        //String textLongitude = editTextLng.getText().toString();

        String[] points_lat = {"-5.869228", "-5.867862", "-5.866859", "-5.866282", "-5.865194", "-5.862760", "-5.860754", "-5.859025", "-5.856976"};
        String[] points_lng = {"-35.181886", "-35.182401", "-35.182037", "-35.181350", "-35.180749", "-35.180899", "-35.181200", "-35.181500", "-35.181779"};


        Intent toThemapsIntent = new Intent(getApplicationContext(),MapsActivity.class);

        //System.out.println("main act lat: " + textLatitude);
        //System.out.println("main act lng: " + textLongitude);

        //toThemapsIntent.putExtra("lat_sent", textLatitude);
        //toThemapsIntent.putExtra("lng_sent", textLongitude);

        toThemapsIntent.putExtra("lat_sent", points_lat);
        toThemapsIntent.putExtra("lng_sent", points_lng);


        startActivity(toThemapsIntent);

    }

}
