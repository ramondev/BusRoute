package org.oakz.busroute;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        /*

        Intent myIntent = this.getIntent();

        String lat_string = myIntent.getStringExtra("lat_sent");
        String lng_string = myIntent.getStringExtra("lng_sent");

        double lat_d = Double.parseDouble(lat_string);
        double lng_d = Double.parseDouble(lng_string);

        */

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {


        Intent myIntent = getIntent();

        //String lat_string = myIntent.getStringExtra("lat_sent");
        String[] lat_string = myIntent.getStringArrayExtra("lat_sent");
        //String lng_string = myIntent.getStringExtra("lng_sent");
        String[] lng_string = myIntent.getStringArrayExtra("lng_sent");


        System.out.println("map act lat: " + lat_string);
        System.out.println("map act lng: " + lng_string);

        //double lat_d = Double.parseDouble(lat_string);
        //double lng_d = Double.parseDouble(lng_string);


        double[] lat_d = {0, 1, 2, 3, 4, 5, 6, 7, 8};
        double[] lng_d = {0, 1, 2, 3, 4, 5, 6, 7, 8};

        for (int i = 0; i < 9; i++) {
            lat_d[i] = Double.parseDouble(lat_string[i]);
            lng_d[i] = Double.parseDouble(lng_string[i]);
        }


        mMap = googleMap;


        for (int i = 0; i < 9; i++) {

            // Add a marker in Sydney and move the camera
            //LatLng sydney = new LatLng(-34, 151);
            LatLng sydney = new LatLng(lat_d[i], lng_d[i]);
            mMap.addMarker(new MarkerOptions().position(sydney).title("Testing it"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        }


        // Add a marker in Sydney and move the camera
        ////LatLng sydney = new LatLng(-34, 151);
        //LatLng sydney = new LatLng(lat_d, lng_d);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
